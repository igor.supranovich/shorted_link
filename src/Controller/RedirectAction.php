<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\LinkRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/{hash}', name: 'redirect')]
final class RedirectAction extends AbstractController
{
    public function __construct(
        private LinkRepositoryInterface $linkRepository,
    ) {
    }

    public function __invoke(Request $request, string $hash): Response
    {
        try {
            $link = $this->linkRepository->getByHash($hash);
        } catch (\Throwable) {
            throw new NotFoundHttpException();
        }

        return $this->redirect($link->getLink());
    }
}

