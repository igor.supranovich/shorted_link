<?php

declare(strict_types=1);

namespace App\Controller;

use App\Exception\InvalidLinkException;
use App\Form\ShortLinkFormType;
use App\Service\ShortLinkService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[Route('/', name: 'shortLink')]
final class ShortLinkAction extends AbstractController
{
    public function __construct(
        private ShortLinkService $shortLinkService,
    ) {
    }

    public function __invoke(Request $request): Response
    {
        $form = $this->createForm(ShortLinkFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $hash = $this->shortLinkService->shortLink($form->get('link')->getData());
                $shortedUrl = $this->generateUrl(
                    'redirect',
                    ['hash' => $hash],
                UrlGeneratorInterface::ABSOLUTE_URL,
                );

                $this->addFlash('success', sprintf('Your link: %s', $shortedUrl));
            } catch (InvalidLinkException) {
                $this->addFlash('error', 'Check Your link, your link is not valid');
            }
        }

        return $this->render(
            'short_link_form.html.twig',
            [
                'shortLink' => $form->createView(),
            ],
        );
    }
}

