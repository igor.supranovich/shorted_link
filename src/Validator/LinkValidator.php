<?php

declare(strict_types=1);

namespace App\Validator;

use App\Exception\InvalidLinkException;

final class LinkValidator
{
    public function validateUrl(string $link): void
    {
        if (false === filter_var($link, FILTER_VALIDATE_URL)) {
            throw InvalidLinkException::LinkIsNotValid();
        }
    }
}