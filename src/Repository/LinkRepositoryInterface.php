<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Link;
use App\Exception\LinkNotFoundException;

interface LinkRepositoryInterface
{
    /**
     * @throws LinkNotFoundException
     */
    public function getByHash(string $hash): Link;

    public function save(Link $link): void;
}

