<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Link;
use App\Exception\LinkNotFoundException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class LinkRepository extends ServiceEntityRepository implements LinkRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Link::class);
    }

    public function getByHash(string $hash): Link
    {
        $qb = $this->createQueryBuilder('link');
        $link = $qb
            ->andWhere('link.hash = :hash')
            ->setParameter('hash', $hash)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $link) {
            throw LinkNotFoundException::byHash($hash);
        }

        return $link;
    }

    public function save(Link $link): void
    {
        $this->_em->persist($link);
        $this->_em->flush();
    }
}

