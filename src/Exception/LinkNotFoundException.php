<?php

declare(strict_types=1);

namespace App\Exception;

final class LinkNotFoundException extends \RuntimeException
{
    public static function byHash(string $hash): self
    {
        return new self(sprintf('Link not found by hash "%s"', $hash));
    }
}

