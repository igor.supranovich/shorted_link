<?php

declare(strict_types=1);

namespace App\Exception;

final class InvalidLinkException extends \RuntimeException
{
    public static function LinkIsNotValid(): self
    {
        return new self('Link Is Not Valid');
    }
}

