<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Link;
use App\Repository\LinkRepositoryInterface;
use App\Validator\LinkValidator;

final class ShortLinkService
{
    public function __construct(
        private LinkValidator $urlValidator,
        private LinkRepositoryInterface $linkRepository,
    ) {
    }

    public function shortLink(string $link): string
    {
        $this->urlValidator->validateUrl($link);

        $hash = uniqid();
        $shortLink = new Link($link, $hash);

        $this->linkRepository->save($shortLink);

        return $hash;
    }
}